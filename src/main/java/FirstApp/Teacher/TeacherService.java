package FirstApp.Teacher;


import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class TeacherService {
    private TeacherRepository teacherRepository;
    private TeacherService(TeacherRepository teacherRepository) {
        this.teacherRepository=teacherRepository;
    }
    public void create(Teacher teacher) {
        teacherRepository.save(teacher);
    }
        public Teacher get(int id) {
            return teacherRepository.findById(id).get();
        }

        public void update(Teacher teacher) {
            teacherRepository.save(teacher);
        }

        public void delete(int id) {
            teacherRepository.deleteById(id);
        }

        public List<Teacher> getAll(){
        return teacherRepository.findAll();

        }
}